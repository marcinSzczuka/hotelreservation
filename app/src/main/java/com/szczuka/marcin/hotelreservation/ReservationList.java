package com.szczuka.marcin.hotelreservation;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.szczuka.marcin.hotelreservation.Domain.Reservation;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class ReservationList extends AppCompatActivity {

    private ObjectMapper mapper = new ObjectMapper();
    private ListView reservationListView;
    private ArrayAdapter<Reservation> adapter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        reservationListView = (ListView) findViewById(R.id.reservationList);

        Bundle b = getIntent().getExtras();
        List<Reservation> reservationList = null;
        String reservationListJson = null;
        if(b != null) {
            reservationListJson = b.getString("reservations");
        }

        try {
            final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            mapper.setDateFormat(df);
            reservationList = mapper.readValue(reservationListJson, new TypeReference<List<Reservation>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }
        adapter = new ArrayAdapter<Reservation>(this, R.layout.list_layout, reservationList);
        reservationListView.setAdapter(adapter);

    }
}
