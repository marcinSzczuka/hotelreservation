package com.szczuka.marcin.hotelreservation;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.szczuka.marcin.hotelreservation.Domain.Address;
import com.szczuka.marcin.hotelreservation.Domain.Person;
import com.szczuka.marcin.hotelreservation.Domain.Reservation;
import com.szczuka.marcin.hotelreservation.Domain.Room;
import com.szczuka.marcin.hotelreservation.Domain.Service;
import com.szczuka.marcin.hotelreservation.R;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class AddReservation extends AppCompatActivity implements View.OnClickListener {

    private ListView reservationListView;
    private ArrayAdapter<Reservation> adapter ;
    private Button deleteButton;
    private Button updateButton;

    private List<Integer> reservationIdList;
    private List<Reservation> reservationList = new ArrayList<>();
    private List<Person> personList;
    private List<Address> addressList;
    private List<Room> roomList;
    private List<Service> serviceList;

    private ObjectMapper mapper = new ObjectMapper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_reservation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        reservationListView = (ListView) findViewById(R.id.reservationList);
        updateButton = (Button) findViewById(R.id.updateButton);
        deleteButton = (Button) findViewById(R.id.deleteButton);
        updateButton.setOnClickListener(this);
        deleteButton.setOnClickListener(this);
        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        mapper.setDateFormat(df);

        Bundle b = getIntent().getExtras();
        try {
            String pIds = b.getString("addReservationId");
            reservationIdList = mapper.readValue(pIds, new TypeReference<List<Integer>>(){});
            for(Integer id: reservationIdList) {
                // get reservation object
                String getReservationJson = new String(IOUtils.toByteArray(getAssets().open("getReservationById.json")));
                getReservationJson = getReservationJson.replace("$id", String.valueOf(id));
                String reservationJson = new DownloadWebpageTask().execute("http://10.0.2.2:8080/api/select", "", getReservationJson, "POST").get();
                List<Reservation> tempReservation = mapper.readValue(reservationJson, new TypeReference<List<Reservation>>(){});
                Reservation reservation = tempReservation.get(0);

                // get person object for reservation
                String getPersonJson = new String(IOUtils.toByteArray(getAssets().open("getPersonById.json")));
                getPersonJson = getPersonJson.replace("$id", String.valueOf(reservation.getPersonId()));
                String personsJson = new DownloadWebpageTask().execute("http://10.0.2.2:8080/api/select", "", getPersonJson, "POST").get();
                List<Person> tempPerson = mapper.readValue(personsJson, new TypeReference<List<Person>>(){});
                Person person = tempPerson.get(0);

                // attach address object to person
                String getAddressJson = new String(IOUtils.toByteArray(getAssets().open("getAddressById.json")));
                getAddressJson = getAddressJson.replace("$id", String.valueOf(person.getAddressId()));
                String addressJson = new DownloadWebpageTask().execute("http://10.0.2.2:8080/api/select", "", getAddressJson, "POST").get();
                List<Address> tempAddress = mapper.readValue(addressJson, new TypeReference<List<Address>>(){});
                person.setAddress(tempAddress.get(0));

                // attach person object to reservation
                reservation.setPerson(person);

                // attach room object to reservation
                String getRoomJson = new String(IOUtils.toByteArray(getAssets().open("getRoomById.json")));
                getRoomJson = getRoomJson.replace("$id", String.valueOf(reservation.getRoomId()));
                String roomJson = new DownloadWebpageTask().execute("http://10.0.2.2:8080/api/select", "", getRoomJson, "POST").get();
                List<Room> tempRoom = mapper.readValue(roomJson, new TypeReference<List<Room>>(){});
                reservation.setRoom(tempRoom.get(0));

                // attach service object list
                String getServicesJson = new String(IOUtils.toByteArray(getAssets().open("getServiceByReservationId.json")));
                getServicesJson = getServicesJson.replace("$id", String.valueOf(reservation.getId()));
                String servicesJson = new DownloadWebpageTask().execute("http://10.0.2.2:8080/api/select", "", getServicesJson, "POST").get();
                List<Service> serviceList = mapper.readValue(servicesJson, new TypeReference<List<Service>>(){});
                reservation.setServiceList(serviceList);

                // add reservation to list
                reservationList.add(reservation);
            }
            adapter = new ArrayAdapter<Reservation>(this, R.layout.list_layout, reservationList);
            reservationListView.setAdapter(adapter);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.updateButton:
                try {
                    String jsonString = new String(IOUtils.toByteArray(getAssets().open("serviceUpdate.json")));
                    jsonString = jsonString.replace("$id", String.valueOf(reservationList.get(0).getServiceList().get(0).getId()));
                    new DownloadWebpageTask().execute("http://10.0.2.2:8080/api/insert/update", "", jsonString, "PUT").get();
                    Intent intent = new Intent(this, AddReservation.class);
                    Bundle b = new Bundle();
                    b.putString("addReservationId", "["+String.valueOf(reservationList.get(0).getId())+"]"); //Your id
                    intent.putExtras(b); //Put your id to your next Intent
                    startActivity(intent);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;

            case R.id.deleteButton:
                try {
                    String jsonString = new String(IOUtils.toByteArray(getAssets().open("serviceUpdate.json")));
                    jsonString = jsonString.replace("$id", String.valueOf(reservationList.get(0).getServiceList().get(0).getId()));
                    new DownloadWebpageTask().execute("http://10.0.2.2:8080/api/insert/delete", "", jsonString, "POST").get();
                    Intent intent = new Intent(this, AddReservation.class);
                    Bundle b = new Bundle();
                    b.putString("addReservationId", "["+String.valueOf(reservationList.get(0).getId())+"]"); //Your id
                    intent.putExtras(b); //Put your id to your next Intent
                    startActivity(intent);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    private class DownloadWebpageTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            // params comes from the execute() call: params[0] is the url.
            try {
                return doPost(urls[0], urls[1], urls[2], urls[3]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {

        }
    }

    private String doPost(String s_url, String jsonFileName, String jsonString, String method) throws IOException, JSONException {

        DataOutputStream printout;
        DataInputStream input;
        URL url = new URL(s_url);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod(method);
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestProperty("Content-Type", "application/json");
        conn.connect();

        // Send POST output.
        printout = new DataOutputStream(conn.getOutputStream());
        byte[] data = null;
        if (!jsonFileName.isEmpty()) {
            data = IOUtils.toByteArray(getAssets().open(jsonFileName));
        }
        if(!jsonString.isEmpty()) {
            data = jsonString.getBytes();
        }
        printout.write(data);
        printout.flush ();
        printout.close ();

        // get data from server
        int status = conn.getResponseCode();

        switch (status) {
            case 200:
            case 201:
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(),"utf-8"),8);
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line =reader.readLine()) != null) {
                    sb.append(line);
                }
                reader.close();
                return sb.toString();
        }
        return null;
    }

}
