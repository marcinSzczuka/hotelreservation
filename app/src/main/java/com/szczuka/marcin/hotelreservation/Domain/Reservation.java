package com.szczuka.marcin.hotelreservation.Domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

/**
 * Created by marcin on 12.06.16.
 */
public class Reservation {
    private int id;
    @JsonProperty("room")
    private int roomId;
    @JsonProperty("person")
    private int personId;
    private Date from;
    private Date to;
    private Person person;
    private Room room;
    private List<Service> serviceList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public List<Service> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<Service> serviceList) {
        this.serviceList = serviceList;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + id +
                ", roomId=" + roomId +
                ", personId=" + personId +
                ", from=" + from +
                ", to=" + to +
                ", person=" + (person==null?"null":person.toString()) +
                ", room=" + (room==null?"null":room.toString()) +
                ", serviceList=" + (serviceList==null?"null":serviceList.toString())+
                '}';
    }
}
