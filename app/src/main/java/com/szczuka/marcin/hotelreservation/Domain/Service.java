package com.szczuka.marcin.hotelreservation.Domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Created by marcin on 14.06.16.
 */
public class Service {
    private Integer id;
    @JsonProperty("reservation")
    private Integer reservationId;
    private Date from;
    private Date to;
    private Integer quantity;
    private String type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getReservationId() {
        return reservationId;
    }

    public void setReservationId(Integer reservationId) {
        this.reservationId = reservationId;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Service{" +
                "id=" + id +
                ", reservationId=" + reservationId +
                ", from=" + from +
                ", to=" + to +
                ", quantity=" + quantity +
                ", type='" + type + '\'' +
                '}';
    }
}
