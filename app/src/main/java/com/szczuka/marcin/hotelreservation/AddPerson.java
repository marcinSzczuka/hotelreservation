package com.szczuka.marcin.hotelreservation;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StringArrayDeserializer;
import com.szczuka.marcin.hotelreservation.Domain.Address;
import com.szczuka.marcin.hotelreservation.Domain.Person;
import com.szczuka.marcin.hotelreservation.Domain.Reservation;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class AddPerson extends AppCompatActivity {

    private ListView personListView;
    private ArrayAdapter<Person> adapter ;

    private List<Integer> personIdList;
    private List<Person> personList = new ArrayList<>();
    private List<Address> addressList;
    private ObjectMapper mapper = new ObjectMapper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_person);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        personListView = (ListView) findViewById(R.id.listView);


        Bundle b = getIntent().getExtras();
        try {
            String pIds = b.getString("addPersonId");
            personIdList = mapper.readValue(pIds, new TypeReference<List<Integer>>(){});
            for(Integer id: personIdList) {
                // get person object
                String getPersonJson = new String(IOUtils.toByteArray(getAssets().open("getPersonById.json")));
                getPersonJson = getPersonJson.replace("$id", String.valueOf(id));
                String personsJson = new DownloadWebpageTask().execute("http://10.0.2.2:8080/api/select", "", getPersonJson, "POST").get();
                List<Person> temp = mapper.readValue(personsJson, new TypeReference<List<Person>>(){});
                Person person = temp.get(0);

                // attach address object to person
                String getAddressJson = new String(IOUtils.toByteArray(getAssets().open("getAddressById.json")));
                getAddressJson = getAddressJson.replace("$id", String.valueOf(person.getAddressId()));
                String addressJson = new DownloadWebpageTask().execute("http://10.0.2.2:8080/api/select", "", getAddressJson, "POST").get();
                List<Address> tempAddress = mapper.readValue(addressJson, new TypeReference<List<Address>>(){});
                person.setAddress(tempAddress.get(0));

                // add person to list
                personList.add(person);
            }
            adapter = new ArrayAdapter<Person>(this, R.layout.list_layout, personList);
            personListView.setAdapter(adapter);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private class DownloadWebpageTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            // params comes from the execute() call: params[0] is the url.
            try {
                return doPost(urls[0], urls[1], urls[2], urls[3]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {

        }
    }

    private String doPost(String s_url, String jsonFileName, String jsonString, String method) throws IOException, JSONException {

        DataOutputStream printout;
        DataInputStream input;
        URL url = new URL(s_url);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod(method);
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestProperty("Content-Type", "application/json");
        conn.connect();

        // Send POST output.
        printout = new DataOutputStream(conn.getOutputStream());
        byte[] data = null;
        if (!jsonFileName.isEmpty()) {
            data = IOUtils.toByteArray(getAssets().open(jsonFileName));
        }
        if(!jsonString.isEmpty()) {
            data = jsonString.getBytes();
        }
        printout.write(data);
        printout.flush ();
        printout.close ();

        // get data from server
        int status = conn.getResponseCode();

        switch (status) {
            case 200:
            case 201:
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(),"utf-8"),8);
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line =reader.readLine()) != null) {
                    sb.append(line);
                }
                reader.close();
                return sb.toString();
        }
        return null;
    }

}
