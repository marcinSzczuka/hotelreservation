package com.szczuka.marcin.hotelreservation;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class MainActivity extends Activity implements View.OnClickListener {

    private Button searchButton;
    private Button addPerson;
    private Button reserve;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity);

        searchButton = (Button) findViewById(R.id.searchButton);
        searchButton.setOnClickListener(this);

        addPerson = (Button) findViewById(R.id.addPerson);
        addPerson.setOnClickListener(this);

        reserve = (Button) findViewById(R.id.reserve);
        reserve.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.searchButton:
                try {
                    String data = new DownloadWebpageTask().execute("http://10.0.2.2:8080/api/select", "reservation.json", "POST").get();
                    Intent intent = new Intent(MainActivity.this, ReservationList.class);
                    Bundle b = new Bundle();
                    b.putString("reservations", data); //Your id
                    intent.putExtras(b); //Put your id to your next Intent
                    startActivity(intent);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.addPerson:
                try {
                    String data = new DownloadWebpageTask().execute("http://10.0.2.2:8080/api/insert", "addPerson.json", "PUT").get();
                    Intent intent = new Intent(MainActivity.this, AddPerson.class);
                    Bundle b = new Bundle();
                    b.putString("addPersonId", data);
                    intent.putExtras(b);
                    startActivity(intent);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.reserve:
                try {
                    String data = new DownloadWebpageTask().execute("http://10.0.2.2:8080/api/insert", "addFullReservation.json", "PUT").get();
                    Intent intent = new Intent(MainActivity.this, AddReservation.class);
                    Bundle b = new Bundle();
                    b.putString("addReservationId", data);
                    intent.putExtras(b);
                    startActivity(intent);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                break;

        }
    }

    private class DownloadWebpageTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            // params comes from the execute() call: params[0] is the url.
            try {
                return doPost(urls[0], urls[1], urls[2]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {

        }
    }

    private String doPost(String s_url, String jsonFileName, String method) throws IOException, JSONException {

        DataOutputStream printout;
        DataInputStream input;
        URL url = new URL(s_url);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        //conn.setReadTimeout(10000);
        //conn.setConnectTimeout(15000);
        conn.setRequestMethod(method);
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestProperty("Content-Type","application/json");
        conn.connect();
        //conn.setRequestProperty("Host", "android.");


        // Send POST output.
        printout = new DataOutputStream(conn.getOutputStream ());
        byte[] data= IOUtils.toByteArray(getAssets().open(jsonFileName));
        printout.write(data);
        printout.flush ();
        printout.close ();


        // get data from server
        int status = conn.getResponseCode();

        switch (status) {
            case 200:
            case 201:
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(),"utf-8"),8);
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line =reader.readLine()) != null) {
                    sb.append(line);
                }
                reader.close();
                return sb.toString();
        }
        return null;
    }

}